﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;

namespace giphy_cat
{
    public partial class Form1 : Form
    {
        private readonly RestClient _client;
        public Form1()
        {
            InitializeComponent();
            _client = new RestClient("http://api.giphy.com");
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Btnsearchclick_Click(object sender, EventArgs e)
        {
            var request = new RestRequest("v1/gifs/random");
            request.AddParameter("tag", txtPhrase.Text);
            request.AddParameter("api_key", "");

            var response = _client.Execute<Giphy>(request);

            if (response.IsSuccessful)
            {
                pictureBoxGiphy.Load(response.Data.Data.Images.Original_still.Url);
            }
            else
            {
                MessageBox.Show(response.StatusDescription);
            }
        }
    }
    class Giphy
    {
        public GiphyData Data { get; set; }
    }
    class GiphyData
    {
        public GiphyDataImages Images { get; set; }
    }
    class GiphyDataImages
    {
        public GiphyDataImagesOriginal Original_still { get; set; }
    }
    class GiphyDataImagesOriginal
    {
        public string Url { get; set; }
    }
}
